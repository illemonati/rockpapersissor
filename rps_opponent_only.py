import keras
import tensorflow.keras as keras
import tensorflow as tf
import os
tf.logging.set_verbosity(tf.logging.ERROR)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import numpy as np
import random
import sys


def get_counter(rps: str):
    return {'r': 'p', 's': 'r', 'p': 's'}[rps]


def build_model(x, y):
    model = keras.models.Sequential([
        keras.layers.LSTM(729, input_shape=(x.shape[1], x.shape[2])),
        keras.layers.Dense(y.shape[1], activation='softmax'),
    ])
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['acc'])
    return model


def serialize_rps(rps: str) -> int:
    rps_dic = {'r': 0, 'p': 1, 's': 2}
    return rps_dic[rps]


def deserialize_rps(rps: int) -> str:
    rps_dic = {'r': 0, 'p': 1, 's': 2}
    return [key for key in rps_dic.keys() if rps_dic[key] == rps][0]


def one_hot(rps: int):
    if rps == 0:
        return [1, 0, 0]
    elif rps == 1:
        return [0, 1, 0]
    else:
        return [0, 0, 1]


def gen_x_y(dataset, seq_len):
    x, y = [], []
    for i in range(len(dataset) - seq_len):
        seq_in = dataset[i:i+seq_len]
        seq_out = dataset[i+seq_len]
        x.append([serialize_rps(rps) for rps in seq_in])
        y.append(serialize_rps(seq_out))
    x = np.reshape(x, (len(x), seq_len, 1))
    x = x/3
    y = np.array([one_hot(i) for i in y])
    return x, y


def train(model, x, y, verbose=1):
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20, min_delta=0.0001, restore_best_weights=True)
    model.fit(x, y, epochs=500, verbose=verbose, callbacks=[callback])


def gen_next(model, dataset, seq_len):
    pattern_rps = dataset[-seq_len:]
    pattern = [serialize_rps(rps) for rps in pattern_rps]
    x = np.reshape(pattern, (1, seq_len, 1))
    prediction = model.predict(x)
    prediction_rps = deserialize_rps(int(np.argmax(prediction)))
    # print(prediction)
    return prediction_rps, prediction


def get_move():
    while True:
        try:
            opponent_move = serialize_rps(str(input("Please input move: \n")))
            return deserialize_rps(opponent_move)
        except Exception as e:
            print("You caused exception: {}".format(e))
            print("Please try again")


def get_seq_len(dataset_len):
    if dataset_len > 10:
        return 5
    if dataset_len > 1:
        return dataset_len - 1
    else:
        return -1


#  best move is different from counter since we have a probability rather than a definite next move
def get_best_move(move_possibilities):
    best_counter = {
        ('r', 'p'): 'p',
        ('r', 's'): 'r',
        ('p', 's'): 's'
    }
    most_likely_moves = np.argsort(-move_possibilities)[0]
    most_likely = sorted([most_likely_moves[0], most_likely_moves[1]])
    most_likely = [deserialize_rps(rps) for rps in most_likely]
    return best_counter[tuple(most_likely)]


def helper():
    dataset = []
    print("Input opponents move when it ask you to input move!")
    while True:
        print('___________________________________________')
        opponent_move = get_move()
        dataset.append(opponent_move)
        print("Current dataset: {}".format(dataset))
        seq_len = get_seq_len(len(dataset))
        if not seq_len < 0:
            x, y = gen_x_y(dataset, seq_len)
            model = build_model(x, y)
            train(model, x, y)
            next_move = gen_next(model, dataset, seq_len)
            print("Rock: {:.0%}, Paper: {:.0%}, Scissor: {:.0%}".format(next_move[1][0][0], next_move[1][0][1], next_move[1][0][2]))
            print("If you want to play it safe you should play *(WAY BETTER on average than risky way): {}".format(get_best_move(next_move[1])))
            print("If you want to win big and take risks you should play: {}".format(get_counter(next_move[0])))
        else:
            print("you should play more rounds to get more data")
        print('___________________________________________\n\n')


def play_against_ai():
    dataset = []
    win = {'ai': 0, 'player': 0}
    ai_choice = ''
    print('Ai is not cheating, ai is not determining your move by the move you made this turn. It is determining it by the patterns of previous times. Check source code if you dont belive!')
    print("Input your move when it ask you to input move!")
    while True:
        print('___________________________________________')
        # print("Current dataset: {}".format(dataset))
        print("Current Score: {}".format(win))
        player_move = get_move()
        seq_len = get_seq_len(len(dataset))
        if not seq_len < 0:
            x, y = gen_x_y(dataset, seq_len)
            model = build_model(x, y)
            train(model, x, y)
            player_next = gen_next(model, dataset, seq_len)
            ai_choice = get_best_move(player_next[1])
        else:
            ai_choice = random.choice(['r', 'p', 's'])
        print("Ai move: {}".format(ai_choice))
        if ai_choice == player_move:
            print("TIE!")
        elif get_counter(ai_choice) == player_move:
            print("PLAYER WIN")
            win['player'] += 1
        else:
            print("AI WIN")
            win['ai'] += 1
        dataset.append(player_move)
        print('___________________________________________\n\n')


if __name__ == '__main__':
    try:
        if int(sys.argv[1]) == 1:
            play_against_ai()
        elif int(sys.argv[1]) == 2:
            helper()
    except Exception as e:
        print('Pass 1 for first argument to play against ai')
        print('Pass 2 for first argument to help you play')
